import React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

const HomeScreen = props => {
  const { navigation } = props;
  return (
    <View style={styles.container}>
      <Text>Home Screen</Text>
      <Button
        title='Details'
        onPress={() => {
          navigation.navigate('Details');
        }}
      />
      <Button
        title='Open Drawer'
        onPress={() => {
          navigation.toggleDrawer();
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default HomeScreen;
