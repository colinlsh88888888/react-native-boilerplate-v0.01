import { createDrawerNavigator } from 'react-navigation-drawer';
import HomeScreen from '../screens/shop/HomeScreen';
import DetailsScreen from '../screens/shop/DetailsScreen';
import { createAppContainer } from 'react-navigation';

const DrawerNavigator = createDrawerNavigator({
  Home: {
    screen: HomeScreen
  },
  Details: {
    screen: DetailsScreen
  }
});

export default createAppContainer(DrawerNavigator);
