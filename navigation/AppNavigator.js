import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from '../screens/shop/HomeScreen';
import DetailsScreen from '../screens/shop/DetailsScreen';
import DrawerNavigator from './DrawerNavigator';

import { createAppContainer } from 'react-navigation';

const AppNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
    DrawerNav: DrawerNavigator
  },

  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: 'blue'
      },
      headerTintColor: 'white'
    }
  }
);

export default createAppContainer(AppNavigator);
